  <form action="<?php echo base_url('admin/create_action'); ?>" method="post">
  <div class="col-md-10">
              <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title">Admin</h5>
              </div>  
              <div class="card-body">
                <form>
                  <div class="row">

                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control  col-md-6" name="username" required>
                      </div>
                    </div>

                  <div class="col-md-12 pr-1">
                      <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control  col-md-6" name="password" required>
                      </div>
                  </div>

                  <div class="col-md-12 pr-1">
                      <div class="form-group">
                        <label>Konfirmasi Password</label>
                        <input type="password" class="form-control  col-md-6" name="konfirmasi_password" required>
                      </div>
                  </div>

                  <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>Status Pegawai</label>
                        <br>
                        <input type="radio" value="super_admin" name="status" required> Super Admin
                        <input type="radio" name="status" value="admin" required> Admin
                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button class="btn btn-primary btn-round">Tambah Admin</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>