<link href='https://fonts.googleapis.com/css?family=Open+Sans:700,600' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url('assets/sweetalert/dist/sweetalert2.min.css') ?>" rel="stylesheet" />  
<script src="<?php echo base_url('assets/sweetalert/dist/sweetalert2.all.min.js') ?>"></script>

<style type="text/css">
	body{
  font-family: 'Open Sans', sans-serif;
  background:#3498db;
  margin: 0 auto 0 auto;  
  width:100%; 
  text-align:center;
  margin: 20px 0px 20px 0px;   
}

p{
  font-size:12px;
  text-decoration: none;
  color:#ffffff;
}

h1{
  font-size:1.5em;
  color:#525252;
}

.box{
  background:white;
  width:300px;
  border-radius:6px;
  margin: 0 auto 0 auto;
  margin-top: 100px;
  padding:0px 0px 70px 0px;
  border: #2980b9 4px solid; 
}

.email{
  background:#ecf0f1;
  border: #ccc 1px solid;
  border-bottom: #ccc 2px solid;
  padding: 8px;
  width:250px;
  color:#AAAAAA;
  margin-top:10px;
  font-size:1em;
  border-radius:4px;
}

.password{
  border-radius:4px;
  background:#ecf0f1;
  border: #ccc 1px solid;
  padding: 8px;
  width:250px;
  font-size:1em;
}

.btn{
  background:#2ecc71;
  width:125px;
  padding-top:5px;
  padding-bottom:5px;
  color:white;
  border-radius:4px;
  border: #27ae60 1px solid;
  margin: auto;
  margin-top:20px;
  
  font-weight:800;
  font-size:0.8em;
}

.btn:hover{
  background:#2CC06B; 
}

#btn2{
  float:left;
  background:#3498db;
  width:125px;  padding-top:5px;
  padding-bottom:5px;
  color:white;
  border-radius:4px;
  border: #2980b9 1px solid;
  
  margin-top:20px;
  margin-bottom:20px;
  margin-left:10px;
  font-weight:800;
  font-size:0.8em;
}

#btn2:hover{ 
background:#3594D2; 
}
</style>
<form method="post" action="<?php echo base_url('admin/login_action') ?>">
<div class="box">
<h1>Login</h1>
<h3>SPK BKD.Dawuhanwetan</h3>
<br>
<input type="text" name="username" placeholder="Username" class="email" />
  
<input type="password" name="password" placeholder="Password" class="email" />
  
 
 	<button class="btn center-block">Login</button>

  
</div> <!-- End Box -->
  
</form>


  
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
	function field_focus(field, email)
  {
    if(field.value == email)
    {
      field.value = '';
    }
  }

  function field_blur(field, email)
  {
    if(field.value == '')
    {
      field.value = email;
    }
  }

//Fade in dashboard box
$(document).ready(function(){
    $('.box').hide().fadeIn(1000);
    });

//Stop click event
$('a').click(function(event){
    event.preventDefault(); 
	});
</script>

<?php 
  
if (isset($this->session->userdata['success'])): ?>
  <script type="text/javascript">
    Swal.fire({
        title: "Berhasil !",
        text: '<?php echo $this->session->userdata["success"] ?>',
        type: "success",
    });
  </script>  
<?php
  unset($_SESSION['success']);

 elseif (isset($this->session->userdata['error'])):
?>
  
  <script type="text/javascript">
    Swal.fire({
        title: "Gagal !",
        text: '<?php echo $this->session->userdata["error"] ?>',
        type: "error",
    });
  </script>

<?php 
unset($_SESSION['error']);
endif ?>

