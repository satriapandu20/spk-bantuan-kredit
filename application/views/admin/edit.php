<!-- </html> -->
<!DOCTYPE html>
<html>
<head>
  <title>Form contoh</title>
</head>
<body>
  <form action="<?php echo base_url('admin/update_action'); ?>" method="post">
 <div class="col-md-10">
            <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title">Edit Data Admin</h5>
              </div>
              <div class="card-body">
                <form>
                <input type="hidden" name="id_user" value='<?php echo $id_user ?>'>
                  <div class="row">

                    <div class="col-md-12 pr-1">
                      <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control col-md-6" name="username" value="<?php echo $username ?>" >
                      </div>
                    </div>

                    <div class="col-md-12 pr-1">
                      <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control  col-md-6" name="password">
                      </div>
                  </div>

                  <div class="col-md-12 pr-1">
                      <div class="form-group">
                        <label>Konfirmasi Password</label>
                        <input type="password" class="form-control  col-md-6" name="konfirmasi_password"  >
                      </div>
                  </div>
                 <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>Status Pegawai</label>
                        <br>
                        <?php 
                          $super_admin="";
                          $admin="";

                          if ($status=="admin") 
                          {
                              $admin="checked";   
                          }else{
                              $super_admin="checked";
                          }                   
                         ?>
                        <input type="radio" value="super_admin" name="status" required <?php echo $super_admin; ?>> Super Admin
                        <input type="radio" name="status" value="admin" required <?php echo $admin; ?>> Admin
                        </div>
                    </div>
                  </div>

                    <div class="col-md-12 pr-1">
                    <div class="update ml-auto mr-auto">
                      <button class="btn btn-primary btn-round">Simpan Data</button>
                    </div>
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>