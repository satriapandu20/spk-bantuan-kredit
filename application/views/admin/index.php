<div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Data Admin</h4>
              </div>
              <div class="card-body">
                <a class= "btn btn-primary" id="tambah_admin" href='<?php echo base_url("admin/create") ?>'>Tambah Admin</a>
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                     <th>Nomor</th>
                     
                     <th>Username</th>
                    
                     <th>Status</th>
                     <th>Aksi</th>
                  
                    </thead>
                    <?php 
                    $i= 1;
                    foreach ($admin as $data_admin) { ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        
                        <td><?php echo $data_admin->username?></td>
                     
                        <td><?php echo $data_admin->status ?></td>
                        <td>
                          <a class= "btn btn-warning" href=<?php echo base_url('admin/update/'.$data_admin->id_user) ?>>Edit</a>
                          <a class= "btn btn-danger" onclick="return confirm('Yakin?');" href=<?php echo base_url('admin/delete/'.$data_admin->id_user) ?>>Delete</a>

                        </td>
                      </tr>

                    <?php 
                    $i++;
                    }?>
                  </table>
                </div>
              </div>
            </div>
          </div>



