<!--
=========================================================
 Paper Dashboard 2 - v2.0.0
=========================================================

 Product Page: https://www.creative-tim.com/product/paper-dashboard-2
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard/blob/master/LICENSE)

 Coded by Creative Tim

=========================================================

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->



<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    SISTEM PENDUKUNG KEPUTUSAN 
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  
  <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/css/paper-dashboard.css?v=2.0.0') ?>" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?php echo base_url('assets/demo/demo.css') ?>" rel="stylesheet" />
  <!-- -- -->
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>

  <link href="<?php echo base_url('assets/sweetalert/dist/sweetalert2.min.css') ?>" rel="stylesheet" />  
  <script src="<?php echo base_url('assets/sweetalert/dist/sweetalert2.all.min.js') ?>"></script>
  

</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          <div class="logo-image-small">
            <!-- <img src="../assets/img/logo-small.png"> -->
          </div>
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          Menu
          <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">

          <li>
            <a href="<?php echo base_url('')?>">
              <i class="nc-icon nc-bank"></i>
              <p>Beranda</p>
            </a>
          </li>

          <li>
            <a href="<?php echo base_url('kriteria') ?>">
              <i class="nc-icon nc-bank"></i>
              <p>Kriteria</p>
            </a>
          </li>

          <li>
            <a href="<?php echo base_url('nasabah') ?>">
              <i class="nc-icon nc-bank"></i>
              <p>Nasabah</p>
            </a>
          </li>

          <li>
            <a href="<?php echo base_url('nilai_kriteria') ?>">
              <i class="nc-icon nc-diamond"></i>
              <p>Nilai Kriteria</p>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url('nilai_kriteria/normalisasi') ?>">
              <i class="nc-icon nc-pin-3"></i>
              <p>Hasil</p>
            </a>
          </li>
          <?php if ($this->session->userdata['status']=='super_admin'): ?>
            <li>
            <a href="<?php echo base_url('admin') ?>">
              <i class="nc-icon nc-pin-3"></i>
              <p>Admin</p>
            </a>
          </li>  
          <?php endif ?>
          

            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#pablo">Sistem Pendukung Keputusan Penerimaan Bantuan Kredit BKD. Dawuhanwetan </a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
            
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link btn-rotate" href="<?php echo base_url('admin/logout') ?>">
                
                  <p>
                    Logout                
                     </p>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <!-- <div class="panel-header panel-header-sm">


</div> -->
         <div class="content">
        <div class="row">
      