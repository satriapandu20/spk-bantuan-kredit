 </div>
      </div>
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <nav class="footer-nav">
              <ul>
              </ul>
            </nav>
            <div class="credits ml-auto">
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>

  <!-- <script type="text/javascript">
    test();
    function test()
    {
      console.log('aw');
      Swal.fire('Good Job','asdadwa','success');
    }
  </script> -->
  <!--   Core JS Files   -->
  <script src="<?php echo base_url('assets/js/core/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/core/popper.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/core/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/plugins/perfect-scrollbar.jquery.min.js')?>"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="<?php echo base_url('assets/js/plugins/chartjs.min.js') ?>"></script>
  <!--  Notifications Plugin    -->
  <script src="<?php echo base_url('assets/js/plugins/bootstrap-notify.js') ?>"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url ('assets/js/paper-dashboard.min.js?v=2.0.0') ?>" type="text/javascript"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?php echo base_url('assets/demo/demo.js')?>"></script>

  <script src="<?php echo base_url('assets/sweetalert/sweetalert.js') ?>"></script>




</body>

</html>
  
<?php 
  
if (isset($this->session->userdata['success'])): ?>
  <script type="text/javascript">
    Swal.fire({
        title: "Berhasil !",
        text: '<?php echo $this->session->userdata["success"] ?>',
        type: "success",
    });
  </script>  
<?php
  unset($_SESSION['success']);

 elseif (isset($this->session->userdata['error'])):
?>
  
  <script type="text/javascript">
    Swal.fire({
        title: "Gagal !",
        text: '<?php echo $this->session->userdata["error"] ?>',
        type: "error",
    });
  </script>

<?php 
unset($_SESSION['error']);
endif ?>

