<div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Data Nilai Nasabah</h4>
              </div>
              <div class="card-body">
                <a class= "btn btn-primary" href='<?php echo base_url("nilai_kriteria/create") ?>'>Tambah Nilai Nasabah</a>
               <a class= "btn btn-primary" href='<?php echo base_url("nilai_kriteria/normalisasi") ?>'>Proses Nilai</a>
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                     <th>No</th>
                     <th>NIK</th>
                     <th>Nama</th>

                    <?php 
                    $i= 1;
                    foreach ($kriteria as $kt): ?>
                      <th><?php echo $kt->nama_kriteria ?></th>
                    <?php endforeach ?>
                     <th>Aksi</th>
                    <tbody>
                       <!-- ////////////// -->
                      <?php foreach ($nasabah as $nsb): ?>

                        <tr>
                          <td><?php echo $i;
                           $i++; ?></td>
                          <td><?php echo $nsb->id_nasabah; ?></td>
                          <td> <?php echo $nsb->nama_nasabah; ?></td>

                          <!-- -- -->
                          <?php 
                          $nilai=$this->Nilai_kriteria_model->get_nilai_by_id($nsb->id_nasabah);
                          ?>
                            <?php foreach ($nilai as $nl): ?>
                            <td><?php echo $nl->nilai; ?></td>
                              <?php endforeach ?>

                            <td>
                             <a class= "btn btn-danger" onclick="return confirm('Yakin?');" href=<?php echo base_url('nilai_kriteria/delete/'.$nsb->id_nasabah) ?>>Delete</a>  
                            </td>
                        </tr>
                      <?php endforeach ?> 
                     </tbody>
                     

                  </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>

