<!-- </html> -->
<!DOCTYPE html>
<html>
<head>
  <title>Form Nilai Kriteria</title>
  

</head>
<body>
  <form action="<?php echo base_url('nilai_kriteria/create_action'); ?>" method="post">
  <div class="col-md-30">
            <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title">Form Penilaian Nasabah</h5>
              </div>
              <div class="card-body">
                <form>
                  <div class="row">
                    <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>ID Nasabah</label>
                        <select name="id_nasabah" class="form-control" required>
                                <option value="">- Pilih -</option>
                            <?php 
                            foreach ($nasabah as $nsb): ?>
                         <!-- //// -->
                            <?php
                              $cek=$this->db->query('select*from nilai_kriteria where id_nasabah='.$nsb->nik.'')->row();
                            ?> 

                            <?php if ($cek==null): ?>
                               <option value="<?php echo $nsb->nik; ?>">
                                  <?php echo $nsb->nama_nasabah.' - '.$nsb->nik; ?>
                                </option>
                            <?php endif ?>
                         
                            <?php endforeach;?>
                        </select>
                      </div>
                    </div>
                    

                    <?php 
                      foreach ($kriteria as $kt):
                        $opsi_kriteria= $this->Opsi_kriteria_model->get_all($kt->id_kriteria);
                    ?>

                    <div class="col-md-10 pr-1">
                        <div class="form-group">
                          <label><?php echo $kt->nama_kriteria; ?></label>
                          <select name="<?php echo 'id_kriteria['.$kt->id_kriteria.']' ?>" class="form-control" required>
                            <option value="">- Pilih -</option>
                            <?php foreach ($opsi_kriteria as $ok): ?>
                             <option value= "<?php echo $ok->value?>"><?php echo $ok->opsi_kriteria; ?></option> 
                            <?php endforeach ?>
                            
                           
                          </select>
                        </div>
                      </div>


                   <?php endforeach; ?>         
                  </div>
                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button class="btn btn-primary btn-round">Input Data Nasabah</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>