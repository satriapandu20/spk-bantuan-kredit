<!DOCTYPE html>
<html>
<head>
	<title>Form contoh</title>
</head>
<body>
	<h2>FORM IDENTITAS BUKU</h2>

	<form action="<?php echo base_url('contoh/form_action'); ?>" method="post">

		<label>Nama Buku</label><br>
		<input type="text" name="nama_buku">

		<br>

		<label>Pengarang</label><br>
		<input type="text" name="pengarang">

		<br>

		<label>Tanggal Terbit</label><br>
		<input type="date" name="tanggal_terbit">

		<br>

		<label>Publikasi</label><br>
		<input type="text" name="publikasi">

		<br>

		<label>Keterangan</label><br>
		<input type="text" name="keterangan">
		<br>
		<button>Tambah</button>

	</form>
	
</body>
</html>