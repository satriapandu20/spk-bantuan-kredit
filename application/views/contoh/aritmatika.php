<div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Simple Table</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>
                        Penjumlahan
                      </th>
                      <th>
                        Pengurangan
                      </th>
                      <th>
                        Pembagian 
                      </th>
                      <th class="text-right">
                        Perkalian
                      </th>
                    </thead>
                    <tbody>
                      <tr>
                        <td><?php echo $penjumlahan ?></td>
                        
                        <td>
                        <?php echo $pengurangan ?>
                        </td>
                        <td>
                        <?php echo $pembagian ?>
                        </td>
                        <td>
                        <td class="text-right">
                        <?php echo $perkalian ?>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

<!-- <table width='700' border='1' style="border-collapse: collapse;" cellpadding="10"> -->

	<!-- <tr> -->
		<!-- <th> Penjumlahan </th>
		<th> Pengurangan </th>
		<th> Pembagian </th>
		<th> Perkalian </th>
	</tr>

	<tr>
		<td> <?php echo $penjumlahan ?> </td>
		<td><?php echo $pengurangan ?></td>
		<td><?php echo $pembagian ?></td>
		<td><?php echo $perkalian ?></td>
	</tr>
</table> -->