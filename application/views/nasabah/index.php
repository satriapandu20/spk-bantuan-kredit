<div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Data Nasabah</h4>
              </div>
              <div class="card-body">
                <a class= "btn btn-primary" href='<?php echo base_url("nasabah/create") ?>'>Tambah Nasabah</a>

               <form action="<?php echo base_url('nasabah/index') ?>">
                  <div class="col-md-5 pull-right" >
                  <input type="text" class="form-control" name="search" placeholder="Pencarian.....">
                  </div>
                  <br>
                  <br>
                  <br>
                </form>

                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                     <th>Nomor</th>
                     <th>NIK</th>
                     <th>Nama Nasabah</th>
                     <th>Tanggal Pengajuan</th>
                     <th>Alamat</th>
                     <th>Aksi</th>

                    </thead>
                    <tbody>

                    <?php 
                    $i= 1;
                     foreach($nasabah as $data_nasabah): ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $data_nasabah->nik ?></td>
                        <td><?php echo $data_nasabah->nama_nasabah ?></td>
                        <td><?php echo $data_nasabah->tanggal_pengajuan?></td>
                        <td><?php echo $data_nasabah->alamat ?></td>
                        <td>

                          <a class= "btn btn-warning" href=<?php echo base_url('nasabah/update/'.$data_nasabah->nik) ?>>Edit</a>
                          <a class= "btn btn-danger" onclick="return confirm('Yakin?');" href=<?php echo base_url('nasabah/delete/'.$data_nasabah->nik) ?>>Delete</a>

                        </td>
                      </tr>
                    <?php 
                    $i++;
                    endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

<!-- <table width='700' border='1' style="border-collapse: collapse;" cellpadding="10"> -->

	<!-- <tr> -->
		<!-- <th> Penjumlahan </th>
		<th> Pengurangan </th>
		<th> Pembagian </th>
		<th> Perkalian </th>
	</tr>

	<tr>
		<td> <?php echo $penjumlahan ?> </td>
		<td><?php echo $pengurangan ?></td>
		<td><?php echo $pembagian ?></td>
		<td><?php echo $perkalian ?></td>
	</tr>
</table> -->