<!-- </html> -->
<!DOCTYPE html>
<html>
<head>
  <title>Form contoh</title>
</head>
<body>
  <form action="<?php echo base_url('nasabah/update_action'); ?>" method="post">
  <div class="col-md-30">
            <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title">Edit Data Nasabah</h5>
              </div>
              <div class="card-body">
                <form>
                <input type="hidden" name="nik" value='<?php echo $nik ?>'>
                  <div class="row">

                    <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>NIK</label>
                        <input type="text" class="form-control" name="nik" value='<?php echo $nik ?>' readonly>
                      </div>
                    </div>

                    <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>Nama Nasabah</label>
                        <input type="text" class="form-control" name="nama_nasabah" value='<?php echo $nama_nasabah ?>'>
                      </div>
                    </div>

                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <label>Tanggal Pengajuan</label>
                        <input type="date" class="form-control" name="tanggal_pengajuan" value='<?php echo $tanggal_pengajuan ?>'>
                      </div>
                    </div>

                    <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control"><?php echo $alamat ?></textarea>
                      </div>
                    </div>

                  </div>
                  
                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button class="btn btn-primary btn-round">Simpan Data</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>