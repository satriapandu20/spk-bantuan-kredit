<div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Data Opsi Kriteria <?php echo $kriteria->nama_kriteria; ?></h4>
              </div>
              <div class="card-body">
                <a class= "btn btn-primary" href='<?php echo base_url("kriteria/opsi_create/".$id_kriteria) ?>'>Tambah Opsi</a>
              <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                     <th>No</th>
                     <th>Opsi Kriteria</th>
                     <th>Value</th>
                     <th>Aksi</th>
                     </thead>
                    <tbody>

                    <?php 
                    $i= 1;
                    $cek_bobot=0;
                    $cek_banyak_kriteria=0;
                    foreach($opsi_kriteria as $ok): ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $ok-> opsi_kriteria ?></td>
                        <td><?php echo $ok-> value ?></td>
                        <td>

                        <a class= "btn btn-warning" href=<?php echo base_url('kriteria/opsi_update/'.$ok->id_opsi)?>>Edit</a>
                          <a class= "btn btn-danger" onclick="return confirm('Yakin?');" href=<?php echo base_url('kriteria/delete_opsi_kriteria/'.$ok->id_kriteria.'/'.$ok->id_opsi)?> >Delete</a>
                       </td>
                      </tr>

                  <?php 
                    $i++;
                  endforeach; ?>
                  </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>

