<!-- </html> -->
<!DOCTYPE html>
<html>
<head>
  <title>Form Edit Opsi</title>
</head>
<body>
  <form action="<?php echo base_url("kriteria/opsi_update_action/".$opsi_kriteria->id_kriteria); ?>" method="post">
  <div class="col-md-30">
            <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title">Edit Opsi</h5>
              </div>
              <div class="card-body">
                <form>
                <input type="hidden" name="id_opsi" value='<?php echo $opsi_kriteria->id_opsi ?>'>
                  <div class="row">
                    <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>Opsi Kriteria</label>
                        <input type="text" class="form-control" name="opsi_kriteria" value='<?php echo $opsi_kriteria->opsi_kriteria ?>'>
                      </div>
                    </div>
                  <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>Value</label>
                        <input type="text" class="form-control" name="value" value='<?php echo $opsi_kriteria->value ?>'>
                      </div>
                    </div>
                 </div>

                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button class="btn btn-primary btn-round">Simpan Data</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>