<!-- </html> -->
<!DOCTYPE html>
<html>
<head>
  <title>Form Kriteria</title>
</head>
<body>
  <form action="<?php echo base_url('kriteria/update_action'); ?>" method="post">
  <div class="col-md-30">
            <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title">Edit Data Kriteria</h5>
              </div>
              <div class="card-body">
                <form>
                <input type="hidden" name="id_kriteria" value='<?php echo $id_kriteria ?>'>
                  <div class="row">
                    <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>Nama Kriteria</label>
                        <input type="text" class="form-control" name="nama_kriteria" value='<?php echo $nama_kriteria ?>'>
                      </div>
                    </div>
                  <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>Tingkat Kepentingan</label>
                        <input type="text" class="form-control" name="tingkat_kepentingan" value='<?php echo $tingkat_kepentingan ?>'>
                      </div>
                    </div>
                    <div class="col-md-4 pr-1">
                      <div class="form-group">
                        <label>Bobot</label>
                        <input type="text" class="form-control" name="bobot" value='<?php echo $bobot ?>' readonly>
                      </div>
                    </div>
                    <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>Atribut</label>
                        <br>
                        <input type="radio" value="benefit" name="atribut" required> Benefit
                        <input type="radio" name="atribut" value="cost" required> Cost
                        </div>
                    </div>
                  </div>

                  </div>

                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button class="btn btn-primary btn-round">Simpan Data</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>