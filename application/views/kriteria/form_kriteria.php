<!-- </html> -->

  <form action="<?php echo base_url('kriteria/create_action'); ?>" method="post">
  <div class="col-md-12">
            <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title">Kriteria</h5>
              </div>  
              <div class="card-body">
                <form>
                  <div class="row">
                    <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>Nama Kriteria</label>
                        <input type="text" class="form-control" name="nama_kriteria" required>
                      </div>
                    </div>

                    <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>Tingkat Kepentingan</label>
                        <input type="text" class="form-control  col-md-3" name="tingkat_kepentingan" required>
                      </div>
                    </div>

                   <div class="col-md-10 pr-1">
                      <div class="form-group">
                        <label>Atribut</label>
                        <br>
                        <input type="radio" value="benefit" name="atribut" required> Benefit
                        <input type="radio" name="atribut" value="cost" required> Cost
                        </div>
                    </div>
                  </div>
                  </div>
                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button class="btn btn-primary btn-round">Tambah Kriteria</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>