<div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Data Kriteria</h4>
              </div>
              <div class="card-body">
                <a class= "btn btn-primary" id="tambah_kriteria" href='<?php echo base_url("kriteria/create") ?>'>Tambah Kriteria</a>
                <a href='<?php echo base_url ("kriteria/proses_bobot") ?>' class= "btn btn-success">Proses Bobot</a>
                <a href='<?php echo base_url ("kriteria/reset_bobot") ?>' class= "btn btn-danger">Reset Bobot</a>
                <form action="<?php echo base_url('kriteria/index') ?>">
                  <div class="col-md-5 pull-right" >
                  <input type="text" class="form-control" name="search" placeholder="Pencarian.....">
                  </div>
                  <br>
                  <br>
                  <br>
                </form>
                <div class="table-responsive">
                  <table class="table">
                   <thead class=" text-primary">
                     <th>Nomor</th>
                     <th>Nama Kriteria</th>
                     <th>Tingkat Kepentingan</th>
                     <th>Bobot</th>
                     <th>Aksi</th>
                  
                    </thead>
                    <tbody>

                    <?php 
                    $i= 1;
                    $cek_bobot=0;
                    $cek_banyak_kriteria=0;
                     foreach($kriteria as $data_kriteria): ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $data_kriteria->nama_kriteria ?></td>
                        <td><?php echo $data_kriteria->tingkat_kepentingan ?></td>
                        <td><?php echo $data_kriteria->bobot ?></td>
                        <td>

                          <a class= "btn btn-success" href=<?php echo base_url('kriteria/opsi/'.$data_kriteria->id_kriteria) ?>>Opsi</a>
                          <a class= "btn btn-warning" href=<?php echo base_url('kriteria/update/'.$data_kriteria->id_kriteria)?>>Edit</a>
                          <a class= "btn btn-danger" onclick="return confirm('Yakin?');" href=<?php echo base_url('kriteria/delete/'.$data_kriteria->id_kriteria) ?>>Delete</a>

                        </td>
                      </tr>
                    <?php 
                    $i++;
                    $cek_banyak_kriteria=$cek_banyak_kriteria+1;
                    if ($data_kriteria->bobot!=0) 
                    {
                      $cek_bobot=1;
                    }
                    endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
<script type="text/javascript">
$(document).ready(function(){
    var cek_bobot='<?php echo $cek_bobot ?>';
    var cek_banyak_kriteria='<?php echo $cek_banyak_kriteria ?>';
    if (cek_bobot==1||cek_banyak_kriteria==6){
      $('#tambah_kriteria').attr('disabled',true);
    }
    else {
     $('#tambah_kriteria').attr('disabled',false);
    }


});
</script>
