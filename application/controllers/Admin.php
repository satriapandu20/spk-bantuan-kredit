<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('Admin_model');

		$this->load->library('session');//use session
		$this->load->library('templates');
	}
	public function index()
	{
		$admin=$this->Admin_model->get_all();
		$data= array (
			"admin"=>$admin,
		);

		$this->load->view('templates/header');
		$this->load->view('admin/index.php',$data);
		$this->load->view('templates/footer');


	}

	public function login()
	{
		// var_dump($this->session->userdata);
		$this->load->view('admin/login');
	}
	public function check_login()
	{
		if($this->session->userdata['login']!=true)
		{
			return redirect('admin/login');
		}
	}
	public function login_action()
	{
		$cek_login=$this->Admin_model->login_action();
		
		if($cek_login!=null)
		{
			$data=array(
				'login'=>'true',
				'status'=>$cek_login->status,
			);

			$this->session->set_userdata($data);
			return redirect('');
		}else{
			$this->session->set_userdata('error','Gagal Login !');
			return redirect('admin/login');
		}
	}

	public function logout()
	{
		foreach ($_SESSION as $key => $value) {
			unset($_SESSION[$key]);
		}

		return redirect('admin/login');
	}

	public function create()
	{	
		$this->load->view('templates/header');
		$this->load->view('admin/form_admin.php');
		$this->load->view('templates/footer');
	} 
	public function create_action()
	{/// proses inputnya
		if($this->input->post('password')==$this->input->post('konfirmasi_password'))
		{
			$this->Admin_model->insert_admin();		
			$this->session->set_userdata('success','Berhasil Menambah');	
			redirect(base_url ('admin/index'));

		}else{
			$this->session->set_userdata('error','Password Tidak Sama');
			redirect(base_url('admin/create'));
		}
		

		
	}
	public function update($id_user)
	{
		$data=$this->Admin_model->get_data($id_user);
		$this->load->view('templates/header');
		$this->load->view('admin/edit',$data);
		$this->load->view('templates/footer');
	}
	public function update_action()
	{
		if ($this->input->post('password')!='' && $this->input->post('konfirmasi_password')!='') {
			if($this->input->post('password')==$this->input->post('konfirmasi_password'))
			{
				$this->Admin_model->update_admin();	
				$this->session->set_userdata('success','Berhasil Mengubah');	
				return redirect(base_url ('admin/index'));
			}else{
				echo 'gagal';
			}
		}else{
			$data=$this->Admin_model->update_admin();
			$this->session->set_userdata('success','Berhasil Mengubah');	
			return redirect(base_url ('admin/index'));			
		}
	}

	public function delete($id_user)
	{
		$data=$this->Admin_model->delete_admin($id_user);

		$this->session->set_userdata('success','Berhasil Menghapus');
		return redirect(base_url ('admin/index'));
	}
}
?>
