<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kriteria extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('Kriteria_model');
		$this->load->model('Opsi_kriteria_model');
		$this->load->library('session');
		$this->load->library('templates');
	}
	////fungsi supaya ke halaman login terus jika belum login
	public function check_login()
	{
		if($this->session->userdata['login']!=true)
		{
			return redirect('admin/login');
		}
	}

	public function index()
	{
		$this->check_login();
		//
		$kriteria=$this->Kriteria_model->get_all();
		$keyword=$this->input->get('search');
		if ($keyword != null || $keyword != '') 
		{
			$kriteria=$this->Kriteria_model->search($keyword);
		}	
		$data= array (
		"kriteria"=>$kriteria,
		);
		$this->load->view('templates/header');
		$this->load->view('kriteria/index.php',$data);
		$this->load->view('templates/footer');
	}

	public function create()
	{
		
		$this->load->view('templates/header');
		$this->load->view('kriteria/form_kriteria');
		$this->load->view('templates/footer');
	} // nampil form
	public function create_action()
	{/// proses inputnya
		
		$data=$this->Kriteria_model->insert_kriteria();
		$this->session->set_userdata('success','Berhasil Menambah');	
		return redirect(base_url ('kriteria/index'));

	}

	public function update($id_kriteria)
	{
		$data=$this->Kriteria_model->get_data($id_kriteria);
		$this->load->view('templates/header');
		$this->load->view('kriteria/edit',$data);
		$this->load->view('templates/footer');
	}
	public function update_action()
	{
		$data=$this->Kriteria_model->update_kriteria();
		$this->session->set_userdata('success','Berhasil Menambah');	
		return redirect(base_url ('kriteria/index'));

	}

	public function delete($id_kriteria)
	{
		$data=$this->Kriteria_model->delete_kriteria($id_kriteria);
		$this->session->set_userdata('success','Berhasil Dihapus');	
		return redirect(base_url ('kriteria/index'));
	}

	public function proses_bobot()
	{
		////////////////////
		$data=array();
		//1
		$kriteria=$this->Kriteria_model->get_all();
		///wadah
		foreach ($kriteria as $bawah) {
			foreach($kriteria as $atas)
			{
				$data[$bawah->id_kriteria][$atas->id_kriteria] = 0;
			}
		}


		///perbandingan 1
		foreach ($kriteria as $kt1) {

			foreach($kriteria as $kt2)
			{
				$gap= $kt2->tingkat_kepentingan - $kt1->tingkat_kepentingan;
				$nilai_kt=0;
				
				if($gap==0)
				{
					$nilai_kt=1;
				}else if($gap==1 || $gap==-1)
				{
					$nilai_kt=3;
				}
				else if ($gap==2 || $gap==-2) {
					$nilai_kt=5;
				}
				else if ($gap==3 || $gap==-3) {
					$nilai_kt=7;
				}
				else if ($gap==4 || $gap==-4) {
					$nilai_kt=9;
				}

//////////////////////cekkkkkkkkkkkkkkkkkkkkk
				if ($data[$kt1->id_kriteria][$kt2->id_kriteria]==0) {
						$data[$kt1->id_kriteria][$kt2->id_kriteria]=$nilai_kt;
				}

				if ($data[$kt2->id_kriteria][$kt1->id_kriteria]==0) {
					$data[$kt2->id_kriteria][$kt1->id_kriteria]=1/$data[$kt1->id_kriteria][$kt2->id_kriteria];
				}
					
			}
			
		}



		

		
		//////////////////////////////////
		//total
		$data_total=array();
		foreach ($kriteria as $total_kt1) {
			$data_total[$total_kt1->id_kriteria]=0;

			foreach ($kriteria as $total_kt2) {

				$data_total[$total_kt1->id_kriteria]=$data_total[$total_kt1->id_kriteria]+$data[$total_kt2->id_kriteria][$total_kt1->id_kriteria];
			}

			
		}

		///////////////

		//eigen
		foreach ($kriteria as $eigen1) 
		{
			foreach ($kriteria as $eigen2) 
			{
				$data[$eigen1->id_kriteria][$eigen2->id_kriteria]=$data[$eigen1->id_kriteria][$eigen2->id_kriteria]/$data_total[$eigen2->id_kriteria];
			}
		}

		//////////////
		///jumlah eigen

		$banyak_kt=0;

		foreach ($kriteria as $banyak) 
		{
			$banyak_kt=$banyak_kt+1;
		}
		//
		$datajumlah_eigen= array ();
		$rata_eigen=array();
		foreach ($kriteria as $jumlah1) 
		{
			$datajumlah_eigen[$jumlah1->id_kriteria]=0;

			foreach ($kriteria as $jumlah2) 
			{
				$datajumlah_eigen[$jumlah1->id_kriteria]=$datajumlah_eigen[$jumlah1->id_kriteria]+$data[$jumlah1->id_kriteria][$jumlah2->id_kriteria];
				$rata_eigen[$jumlah1->id_kriteria]=$datajumlah_eigen[$jumlah1->id_kriteria]/$banyak_kt;
			}
		}

		////////////////
		////validasi 
		//mencari lamda max
		$lamdamax=0;
		foreach ($kriteria as $lmd) 
		{
			$lamdamax=$lamdamax+($data_total[$lmd->id_kriteria]*$rata_eigen[$lmd->id_kriteria]);
		}
		//mencari CI
		$ci=($lamdamax-$banyak_kt)/($banyak_kt-1);
		//mencari CR
		$pembagi_cr=0;
		if($banyak_kt==1)
		{
			$pembagi_cr=0.00;
		}
		elseif ($banyak_kt==2) 
		{
			$pembagi_cr=0.00;
		}
		elseif ($banyak_kt==3) 
		{
			$pembagi_cr=0.58;
		}
		elseif ($banyak_kt==4) 
		{
			$pembagi_cr=0.90;
		}
		elseif ($banyak_kt==5)
		{
			$pembagi_cr=1.12;
		}
		else if($banyak_kt==6)
		{
			$pembagi_cr=1.24;
		}

		$cr= $ci/$pembagi_cr;


		///////////////////////
		///////////////////
		////////////////////
		
		if($cr<0.1)
		{
			foreach ($kriteria as $bobot) 
			{
				$update_bobot=$this->Kriteria_model->update_bobot($bobot->id_kriteria, $rata_eigen[$bobot->id_kriteria]);
			}
			// $update_bobot=$this->Kriteria_model->update_bobot();

			return redirect(base_url ('kriteria/index'));
		}else{
			echo 'gagal';
		}
	

	}
	//Reset Bobot
	public function reset_bobot()
	{
		$kriteria=$this->Kriteria_model->get_all();
		foreach ($kriteria as $kt) 
		{
			$this->Kriteria_model->reset_bobot($kt->id_kriteria);

		}
		$this->session->set_userdata('success','Berhasil DiHapus');	
		return redirect(base_url ('kriteria/index'));
		
	}

	public function opsi($id_kriteria)
	{
		$opsi_kriteria=$this->Opsi_kriteria_model->get_all($id_kriteria);
		$kriteria= $this->Kriteria_model->get_data($id_kriteria);
		$data= array (
			"opsi_kriteria"=>$opsi_kriteria,
			"id_kriteria"=>$id_kriteria,
			"kriteria"=>$kriteria,
		);

		$this->load->view('templates/header');
		$this->load->view('opsi_kriteria/index.php',$data);
		$this->load->view('templates/footer');
	}

	public function opsi_create($id_kriteria)
	{
		$data= array (
			"id_kriteria"=>$id_kriteria,
		);
		$this->load->view('templates/header');
		$this->load->view('opsi_kriteria/form_opsi_kriteria.php',$data);
		$this->load->view('templates/footer');
	}

	public function opsi_create_action()
	{/// proses inputnya
		
		$data=$this->Opsi_kriteria_model->insert_opsi_kriteria();
		$this->session->set_userdata('success','Berhasil Menambah');	
		return redirect(base_url ('kriteria/opsi/'.$this->input->post('id_kriteria')));

	}
	public function opsi_update($id_kriteria)
	{
		$opsi_kriteria=$this->Opsi_kriteria_model->get_data($id_kriteria);
		// var_dump($opsi_kriteria);
		// die;
		$data= array (
			"id_kriteria"=>$id_kriteria,
			"opsi_kriteria"=>$opsi_kriteria,
		);
		
		$this->load->view('templates/header');
		$this->load->view('opsi_kriteria/edit.php',$data);
		$this->load->view('templates/footer');
	}
	public function opsi_update_action($id_kriteria)
	{
		$data=$this->Opsi_kriteria_model->update_opsi_kriteria();
		$this->session->set_userdata('success','Berhasil Mengubah');	
		return redirect(base_url ('kriteria/opsi/'.$id_kriteria));
	}

	public function delete_opsi_kriteria($id_kriteria,$id_opsi)
	{
		$data=$this->Opsi_kriteria_model->delete_opsi_kriteria($id_opsi);
		$this->session->set_userdata('success','Berhasil Menghapus');	
		return redirect(base_url ('kriteria/opsi/'.$id_kriteria));
	}
}
