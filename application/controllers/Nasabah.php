<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nasabah extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('Nasabah_model');
		$this->load->library('session');
		$this->load->library('templates');
	}

	public function check_login()
	{
		if($this->session->userdata['login']!=true)
		{
			return redirect('admin/login');
		}
	}
	public function index()
	{
		$this->check_login();
		$nasabah=$this->Nasabah_model->get_all();
		$keyword=$this->input->get('search');
		if ($keyword != null || $keyword != '') 
		{
			$nasabah=$this->Nasabah_model->search($keyword);
		}
		$data= array (
		"nasabah"=>$nasabah,
		);
		$this->load->view('templates/header');
		$this->load->view('nasabah/index.php',$data);
		$this->load->view('templates/footer');
	}

	public function create()
	{
		
		$this->load->view('templates/header');
		$this->load->view('nasabah/form_nasabah');
		$this->load->view('templates/footer');
	} // nampil form
	public function create_action()
	{/// proses inputnya
		
		$data=$this->Nasabah_model->insert_nasabah();
		$this->session->set_userdata('success','Berhasil Menambah');	
		return redirect(base_url ('nasabah/index'));

	}

	public function update($nik)
	{
		$data=$this->Nasabah_model->get_data($nik);
		$this->load->view('templates/header');
		$this->load->view('nasabah/edit',$data);
		$this->load->view('templates/footer');
	}
	public function update_action()
	{
		$data=$this->Nasabah_model->update_nasabah();
		$this->session->set_userdata('success','Berhasil Mengubah');	
		return redirect(base_url ('nasabah/index'));

	}

	public function delete($nik)
	{
		$data=$this->Nasabah_model->delete_nasabah($nik);
		$this->session->set_userdata('success','Berhasil Menghapus');	
		return redirect(base_url ('nasabah/index'));
	}
}