<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai_kriteria extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('Nilai_kriteria_model');
		$this->load->model('Kriteria_model');
		$this->load->model('Nasabah_model');
		$this->load->model('Opsi_kriteria_model');
		$this->load->model('Nilai_akhir_model');
		$this->load->library('session');
		$this->load->library('templates');
	}

	public function check_login()
	{
		if($this->session->userdata['login']!=true)
		{
			return redirect('admin/login');
		}
	}

	public function index()
	{
		$this->check_login();
		$nilai_kriteria=$this->Nilai_kriteria_model->get_all();
		$kriteria=$this->Kriteria_model->get_all();
		$nasabah=$this->Nilai_kriteria_model->get_one();

		$data= array (
		"nilai_kriteria"=>$nilai_kriteria,
		"kriteria"=>$kriteria,
		"nasabah"=>$nasabah,
		);
		$this->load->view('templates/header');
		$this->load->view('nilai_kriteria/index.php',$data);
		$this->load->view('templates/footer');
	}

	public function create()
	{
		$kriteria=$this->Kriteria_model->get_all();
		$nasabah=$this->Nasabah_model->get_all();

		$data= array(
			'kriteria'=>$kriteria,
			'nasabah'=>$nasabah,
		);

		$this->load->view('templates/header');
		$this->load->view('nilai_kriteria/form_nilai_kriteria',$data);
		$this->load->view('templates/footer');
	} // nampil form
	public function create_action()
	{/// proses inputnya
		
		$data=$this->Nilai_kriteria_model->insert_nilai_kriteria();
		$this->session->set_userdata('success','Berhasil Menambahkan');
		return redirect(base_url ('nilai_kriteria/index'));

	}

	public function update($id_kriteria)
	{
		$data=$this->Nilai_kriteria_model->get_data($id_kriteria);
		$this->load->view('templates/header');
		$this->load->view('nilai_kriteria/edit',$data);
		$this->load->view('templates/footer');
	}
	public function update_action()
	{
		$data=$this->Nilai_kriteria_model->update_nilai_kriteria();
		$this->session->set_userdata('success','Berhasil Mengubah');
		return redirect(base_url ('nilai_kriteria/index'));

	}

	public function delete($id_nasabah)
	{
		$data=$this->Nilai_kriteria_model->delete_nilai_kriteria($id_nasabah);
		$data=$this->Nilai_akhir_model->delete_nilai($id_nasabah);
		return redirect(base_url ('nilai_kriteria/index'));
	}
	public function normalisasi()
	{
		$nilai_nasabah=array();
		$nilai_kriteria=$this->Nilai_kriteria_model->get_all();
		foreach ($nilai_kriteria as $nk) 
		{
			$kriteria=$this->Kriteria_model->get_data($nk->id_kriteria);
			if ($kriteria->atribut=='Benefit') 
			{
				$nilai_max=$this->Nilai_kriteria_model->get_max($nk->id_kriteria);
				$nilai_nasabah[$nk->id_nasabah][$nk->id_kriteria]=$nk->nilai/$nilai_max->nilai;

			}else{
				$nilai_min=$this->Nilai_kriteria_model->get_min($nk->id_kriteria);
				$nilai_nasabah[$nk->id_nasabah][$nk->id_kriteria]=$nilai_min->nilai/$nk->nilai;
			}	
		}
	///////mendefinisikan nilai akhir
		$nilai_akhir=array();

		foreach ($nilai_nasabah as $key => $value) {
			$nilai_akhir[$key]=0;
		}
		//

		$kriteria=$this->Kriteria_model->get_all();
		foreach ($nilai_nasabah as $key => $value) {
			foreach ($kriteria as $kt) {
				$nilai_akhir[$key]=$nilai_akhir[$key]+ ($nilai_nasabah[$key][$kt->id_kriteria]*$kt->bobot);			
			}
		}
		// var_dump($nilai_akhir);
		foreach ($nilai_akhir as $key => $value) {
			$cek=$this->Nilai_akhir_model->get_data($key);
		
			if($cek==null){
				$this->Nilai_akhir_model->insert_nilai_akhir($key,$value);
			}
			else{
				$this->Nilai_akhir_model->update_nilai_akhir($key,$value);
			}
		}	

		$nilai_nasabah=$this->Nilai_akhir_model->get_all();
		$data= array(
			'nilai_nasabah'=> $nilai_nasabah,
		);

		$this->load->view('templates/header');
		$this->load->view('laporan/index',$data);
		$this->load->view('templates/footer');
	}

}
