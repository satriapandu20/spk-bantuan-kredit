<?php

class Opsi_kriteria_model extends CI_Model{


	public function __construct(){
		$this->load->database();
	}

	public function get_all($id_kriteria){

			$query = $this->db->query('select opsi_kriteria.*,kriteria.nama_kriteria from opsi_kriteria join kriteria on opsi_kriteria.id_kriteria= kriteria.id_kriteria  where opsi_kriteria.id_kriteria='.$id_kriteria.'');
		return $query->result();
	}
	public function get_one(){
		// return $this->db->query('select distinct nilai_kriteria.id_nasabah, nasabah.nama_nasabah from nilai_kriteria join nasabah on nilai_kriteria.id_nasabah=nasabah.nik')->result();

	}
	public function get_nilai_by_id($id_kriteria){
		$this->db->select('*');
		$this->db->where('id_kriteria',$id_kriteria);
		$query=$this->db->get('opsi_kriteria');
		return $query->result();

	}
	public function get_data($id_opsi)
	{	
		$query=$this->db->get_where('opsi_kriteria',array('id_opsi'=>$id_opsi));
		return $query->row();
	}

	public function insert_opsi_kriteria()
	{
		
			$data = array(
				'id_kriteria' =>$this->input->post('id_kriteria'),
				'opsi_kriteria' => $this->input->post('opsi_kriteria'),
				'value' =>$this->input->post('value'),
			);
		
			$this->db->insert('opsi_kriteria', $data);

	
	}
	public function update_opsi_kriteria()
	{
			$data = array(
				'opsi_kriteria' => $this->input->post('opsi_kriteria'),
				'value' =>$this->input->post('value'),
			);
		
		$this->db->where('id_opsi',$this->input->post('id_opsi'));
		return $this->db->update('opsi_kriteria', $data);
	}

	public function delete_opsi_kriteria($id_opsi)
	{
		$this->db->where('id_opsi', $id_opsi);	
		$result=$this->db->delete('opsi_kriteria');
		return $result;
	}


}
?>