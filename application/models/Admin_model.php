<?php

class Admin_model extends CI_Model{


public function __construct(){
		$this->load->database();
	}

public function get_all(){

		$query = $this->db->get('user');
		return $query->result();
	}
public function get_data($id_user)
	{	
		
		
		$query=$this->db->get_where('user',array('id_user'=>$id_user));
		return $query->row();
	}
public function login_action()
{
	$data = array(
	
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
	
	);

	$query=$this->db->get_where('user',array('username'=>$data['username'],'password'=>$data['password']));
		return $query->row();
}

public function insert_admin()
	{
		$data = array(
			'id_user' => $this->input->post('id_user'),
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
			'status' => $this->input->post('status'),
		);

	
		return $this->db->insert('user', $data);
	}
public function update_admin()
	{
		$data=array();
		if ($this->input->post('password')!='' && $this->input->post('konfirmasi_password')!='') {
				$data = array(
					'username' => $this->input->post('username'),
					'password' => md5($this->input->post('password')),
					'status' => $this->input->post('status'),
				);
		}else{
			$data = array(
				'username' => $this->input->post('username'),
				'status' => $this->input->post('status'),
			);
		}

		
		$this->db->where('id_user',$this->input->post('id_user'));
		return $this->db->update('user', $data);
	}
	public function delete_admin($id_user)
	{
		$this->db->where('id_user', $id_user);
		
		$result=$this->db->delete('user');
		return $result;

	}
}
?>