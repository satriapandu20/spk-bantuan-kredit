<?php

class Nilai_akhir_model extends CI_Model{


	public function __construct(){
		$this->load->database();
	}

	public function insert_nilai_akhir($nik,$nilai)
	{
		$data = array(
			'nik'=>$nik,
			'nilai_akhir' => $nilai,
		);
	
		return $this->db->insert('nilai_akhir', $data);
	}

	public function update_nilai_akhir($nik,$nilai)
	{
		$data = array(
			'nilai_akhir' => $nilai,
		);
		
		$this->db->where('nik',$nik);
		return $this->db->update('nilai_akhir', $data);
	}
	public function get_data($nik)
	{	
		$query=$this->db->get_where('nilai_akhir',array('nik'=>$nik));
		return $query->row();
	}
	public function get_all()
	{

		$query = $this->db->query('select nilai_akhir.*,nasabah.nama_nasabah from nilai_akhir join nasabah on nilai_akhir.nik=nasabah.nik order by nilai_akhir desc');

		return $query->result();
	}
	
	public function delete_nilai($nik)
	{
		$this->db->where('nik', $nik);
		
		$result=$this->db->delete('nilai_akhir');
		return $result;
	}

	public function search($keyword){
		$this->db->like('nama_nasabah',$keyword);
		$this->db->or_like('nik',$keyword);
		$query = $this->db->get('nilai_akhir');
		return $query->result();
	}
}

?>