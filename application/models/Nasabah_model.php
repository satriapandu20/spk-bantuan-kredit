<?php

class Nasabah_model extends CI_Model{


	public function __construct(){
		$this->load->database();
	}

	public function get_all(){

		$query = $this->db->get('nasabah');
		return $query->result();
	}

	public function get_data($nik)
	{	
			
		$query=$this->db->get_where('nasabah',array('nik'=>$nik));
		return $query->row();
	}
	
	public function insert_nasabah()
	{
		$data = array(
			'nik' => $this->input->post('nik'),
			'nama_nasabah' => $this->input->post('nama_nasabah'),
			'tanggal_pengajuan' => $this->input->post('tanggal_pengajuan'),
			'alamat' => $this->input->post('alamat'),
		);
	
		return $this->db->insert('nasabah', $data);
	}

	public function update_nasabah()
	{
		$data = array(
			'nik' => $this->input->post('nik'),
			'nama_nasabah' => $this->input->post('nama_nasabah'),
			'tanggal_pengajuan' => $this->input->post('tanggal_pengajuan'),
			'alamat' => $this->input->post('alamat'),
		);
		
		$this->db->where('nik',$this->input->post('nik'));
		return $this->db->update('nasabah', $data);
	}
	public function delete_nasabah($nik)
	{
		$this->db->where('nik', $nik);
		
		$result=$this->db->delete('nasabah');
		return $result;
	}

	public function search($keyword)
	{
		$this->db->like('nama_nasabah',$keyword);
		$this->db->or_like('nik',$keyword);
		$query = $this->db->get('nasabah');
		return $query->result();
	}

}
?>