<?php

class Nilai_kriteria_model extends CI_Model{


	public function __construct(){
		$this->load->database();
	}

	public function get_all(){

		$query = $this->db->get('nilai_kriteria');
		return $query->result();
	}
	public function get_one(){
		return $this->db->query('select distinct nilai_kriteria.id_nasabah, nasabah.nama_nasabah from nilai_kriteria join nasabah on nilai_kriteria.id_nasabah=nasabah.nik')->result();

	}
	public function get_nilai_by_id($id_nasabah){
		$this->db->select('*');
		$this->db->where('id_nasabah',$id_nasabah);
		$query=$this->db->get('nilai_kriteria');
		return $query->result();

	}
	public function get_max($id_kriteria){
		return $this->db->query('select max(nilai) as nilai from nilai_kriteria where id_kriteria='.$id_kriteria)->row();

	}
	public function get_min($id_kriteria){
		return $this->db->query('select min(nilai) as nilai from nilai_kriteria where id_kriteria='.$id_kriteria)->row();
	}
	public function get_data($id_kriteria)
	{	
		
		
		$query=$this->db->get_where('nilai_kriteria',array('id_kriteria'=>$id_kriteria));
		return $query->row();
	}

	public function insert_nilai_kriteria()
	{	
		foreach ($this->input->post('id_kriteria') as $key => $value) {
		$data = array(
				'id_kriteria' => $key,
				'id_nasabah' => $this->input->post('id_nasabah'),
				'nilai' => $value,
			);
		
			$this->db->insert('nilai_kriteria', $data);

		}
		// $data = array(
		// 	'id_kriteria' => $this->input->post('id_kriteria'),
		// 	'id_nasabah' => $this->input->post('id_nasabah'),
		// 	'nilai' => $this->input->post('nilai'),
		// );
	
		// return $this->db->insert('nilai_kriteria', $data);
	}

	public function update_nilai_kriteria()
	{
		$data = array(
			'id_kriteria' => $this->input->post('id_kriteria'),
			'id_nasabah' => $this->input->post('id_nasabah'),
			'nilai' => $this->input->post('nilai'),
		);
		
		$this->db->where('id_kriteria',$this->input->post('id_kriteria'));
		return $this->db->update('nilai_kriteria', $data);
	}

	public function delete_nilai_kriteria($id_nasabah)
	{
		$this->db->where('id_nasabah', $id_nasabah);
		
		$result=$this->db->delete('nilai_kriteria');
		return $result;

	}

	// public function search($keyword){
	// 	$this->db->like('id_kriteria',$keyword);
	// 	$query = $this->db->get('nilai_kriteria');
	// 	return $query->result();
	// }
	


}
?>