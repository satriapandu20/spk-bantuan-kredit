<?php

class Kriteria_model extends CI_Model{


	public function __construct(){
		$this->load->database();
	}

	public function get_all(){

		$query = $this->db->get('kriteria');
		return $query->result();
	}
	
	public function search($keyword){
		$this->db->like('nama_kriteria',$keyword);
		$this->db->or_like('tingkat_kepentingan',$keyword);
		$query = $this->db->get('kriteria');
		return $query->result();
	}

	public function get_data($id_kriteria)
	{	
		
		
		$query=$this->db->get_where('kriteria',array('id_kriteria'=>$id_kriteria));
		return $query->row();
	}

	public function insert_kriteria()
	{
		$data = array(
			'nama_kriteria' => $this->input->post('nama_kriteria'),
			'tingkat_kepentingan' => $this->input->post('tingkat_kepentingan'),
			'atribut' => $this->input->post('atribut'),
		);
	
		return $this->db->insert('kriteria', $data);
	}

	public function update_kriteria()
	{
		$data = array(
			'nama_kriteria' => $this->input->post('nama_kriteria'),
			'tingkat_kepentingan' => $this->input->post('tingkat_kepentingan'),
			'atribut' => $this->input->post('atribut'),
		);
		
		$this->db->where('id_kriteria',$this->input->post('id_kriteria'));
		return $this->db->update('kriteria', $data);
	}

	public function update_bobot($id_kriteria, $bobot)
	{
		$data = array(
			'bobot' => $bobot,
		);
		
		$this->db->where('id_kriteria',$id_kriteria);
		return $this->db->update('kriteria', $data);
	}

	public function delete_kriteria($id_kriteria)
	{
		$this->db->where('id_kriteria', $id_kriteria);
		
		$result=$this->db->delete('kriteria');
		return $result;

	}
	public function reset_bobot($id_kriteria)
	{
		$data = array(
			'bobot' => '0',
		);
		$this->db->where('id_kriteria',$id_kriteria);
		return $this->db->update('kriteria', $data);
	}
}
?>